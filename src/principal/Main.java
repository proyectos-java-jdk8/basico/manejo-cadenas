package principal;

public class Main {

    public static void main(String[] args) {
        String cadena = "Anita lava la tina";
        //contar la cantidad de letras en mayusculas
        //System.out.println(GestorCadena.contarMayusculas(cadena));
        //contar la cantidad de letras en minusculas
        //System.out.println(GestorCadena.contarMinuscula(cadena));
        //contar los espacios en blanco dentro de la cadena
        //System.out.println(GestorCadena.contarEspacios(cadena));
        //contar los números dentro de la cadena
        //System.out.println(GestorCadena.contarNumeros(cadena
        //convertir la cadena a mayusculas
        //System.out.println(GestorCadena.aMayuscula(cadena));
        //convertir cadena a minusculas
        //System.out.println(GestorCadena.aMinuscula(cadena));
        //invertir cadena
        //System.out.println(GestorCadena.invertir(cadena));
        //verificar si una cadena es palindromo
        System.out.println(cadena + " : " + GestorCadena.palindromo(cadena));
    }
    
}
