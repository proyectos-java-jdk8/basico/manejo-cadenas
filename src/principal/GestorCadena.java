package principal;

public class GestorCadena {
    
    public static int contarMayusculas(String cadena){
        int cantidad = 0;
        for(int i=0; i<cadena.length(); i++){
            Character caracter = cadena.charAt(i);
            if(Character.isUpperCase(caracter)){
                cantidad ++;
            } 
        }
        return cantidad;
    }
    
    public static int contarMinuscula(String cadena){
        int cantidad = 0;
        for(int i=0; i<cadena.length(); i++){
            Character caracter = cadena.charAt(i);
            if(Character.isLowerCase(caracter)){
                cantidad ++;
            } 
        }
        return cantidad;
    }
    
    public static int contarEspacios(String cadena){
        int cantidad = 0;
        for(int i=0; i<cadena.length(); i++){
            Character caracter = cadena.charAt(i);
            if(Character.isWhitespace(caracter)){
                cantidad ++;
            } 
        }
        return cantidad;
    }
    
    public static int contarNumeros(String cadena){
        int cantidad = 0;
        for(int i=0; i<cadena.length(); i++){
            Character caracter = cadena.charAt(i);
            if(Character.isDigit(caracter)){
                cantidad ++;
            } 
        }
        return cantidad;
    }
    
    public static String aMayuscula(String cadena){
        return cadena.toUpperCase();
    }
    
    public static String aMinuscula(String cadena){
        return cadena.toLowerCase();
    }
    
    public static String invertir(String cadena){
        String resultado = "";
        for(int i=cadena.length()-1; i>=0; i--){
            resultado += cadena.charAt(i);
        }
        return resultado;
    }
    
    private static String sinEspacios(String cadena){
        String resultado = "";
        for(int i=0; i<cadena.length(); i++){
            char caracter = cadena.charAt(i);
            //si el caracter es una letra o digito
            if(Character.isLetterOrDigit(caracter)){
                resultado += caracter;//concatenar
            }
        }
        return resultado;
    }
    
    public static boolean esPalindromo(String cadena){
        //quitar espacios -> convertir a mayuscula
        String reducida = aMayuscula(sinEspacios(cadena));
        //quitar espacios -> convertir a mayuscula -> invertirla
        String invertida = invertir(aMayuscula(sinEspacios(cadena)));
        return reducida.equals(invertida);//comparar
    }
    
    public static String palindromo(String cadena){
        if(esPalindromo(cadena)){
            return "es palindromo";
        }else{
            return "no es un palindromo";
        }
    }

}
